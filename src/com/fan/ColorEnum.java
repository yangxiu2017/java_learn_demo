package com.fan;

public enum ColorEnum {
	
	RED("red","红色","1"),
	GREEN("green","绿色","2"),
	BLUE("blue","蓝色","3");
	
    //防止字段值被修改，增加的字段也统一final表示常量
    private final String key;
    private final String value;
    private final String temp;
    
    private ColorEnum(String key,String value,String temp){
        this.key = key;
        this.value = value;
        this.temp = temp;
    }
    //根据key获取枚举
    public static ColorEnum getEnumByKey(String key){
        if(null == key){
            return null;
        }
        for(ColorEnum temp:ColorEnum.values()){
            if(temp.getKey().equals(key)){
                return temp;
            }
        }
        return null;
    }
    public String getKey() {
        return key;
    }
    public String getValue() {
        return value;
    }
    public String getTemp(){
    	return temp;
    }

}
