package com.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import sun.applet.Main;

public class ReflectDemo {
	
	public static void main(String[] args) {
		try {
			Class c = Class.forName("com.reflect.Test");
			System.out.println(c.getName());
			Method[] m = c.getDeclaredMethods();
			for (Method  method: m) {
				System.out.println("ALl:"+method.getName());
				if("test1".equals(method.getName())){
					method.invoke(c.newInstance(), new String[]{"3333"});
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void main5(String[] args) {
		Class t = Test.class;
		
		//Field[] f =  t.getDeclaredFields();
		//for (Field field : f) {
		//	System.out.println(field.getName());
		//}
		
		Method[] m = t.getMethods();
		for (Method method : m) {
			System.out.println(method.getName());
		}
	}

	public static void main2(String[] args) {
		// TODO Auto-generated method stub
		Test t = new Test();
		Field[] f =  t.getClass().getDeclaredFields();
		for (Field field : f) {
			System.out.println(field.getName());
		}

	}

}
