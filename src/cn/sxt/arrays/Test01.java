package cn.sxt.arrays;

public class Test01 {

public static  void main(String[]args) {
	  int[] arr01 = new int[10];
	  String[] arr02 = new String[5];
	  User[] arr03 = new User[3];	  
 
   arr01[0] = 13;
   arr01[1] = 15;
   arr01[2] = 20;
  
  //通过循环初始化数组
  for(int i = 0;i<arr01.length;i++) {
    arr01[i] =10*i;
  }
  //通过循环读取数组里面元素的值
  for(int i = 0;i<arr01.length;i++) {
	  System.out.println(arr01[i]);
  }
  
}

class User{
	private int id;
	private String name;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
}
	
	
