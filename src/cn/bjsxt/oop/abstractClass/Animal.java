package cn.bjsxt.oop.abstractClass;
/**
 * 抽象类
 * @author lenovo
 *
 */

public  abstract class Animal {
   public  abstract void run();  //抽象方法 的意义在于，将方法的设计和方法 的实现分离了。
   
   public void breath() {
	   System.out.println("呼吸");
   }
   
   }

class Cat extends Animal{

	@Override
	public void run() {
		System.out.println("猫步小跑");
	}
}
class Dog extends Animal{

	@Override
	public void run() {
		System.out.println("狗跑");
	}
	
}