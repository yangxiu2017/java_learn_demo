package cn.bjsxt.array2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 可视化日历
 * @author lenovo
 *
 */
public class VisualCalendar {
public static void main(String[]args) {
	   String temp = "2040-6-10";
	   DateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
	   try {
		   Date date = formate.parse(temp);
		   Calendar calendar = new GregorianCalendar();
		   calendar.setTime(date);
	   int day = calendar.get(Calendar.DATE);
		   calendar.set(Calendar.DATE,1);
	   
	     System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
	     System.out.println();
	   int maxDate = calendar.getActualMaximum(Calendar.DATE);
	   System.out.println("日\t一\t二\t三\t四\t五\t六");
	   
	   for(int i=1;i<calendar.get(Calendar.DAY_OF_WEEK);i++) {
	     System.out.print('\t');
	   }
	  
	   for(int i =1 ;i<=maxDate;i++) {
		   if(i==day) {
			   System.out.print("*");
		   }
		   
		   System.out.print(i+"\t");
		    int w = calendar.get(Calendar.DAY_OF_WEEK);
		   if(w==Calendar.SATURDAY) {
			   System.out.print('\n'); 
		   }
		   calendar.add(Calendar.DATE,1);
	   }
	   } catch(ParseException e) {
		   e.printStackTrace();
	   }
   
	   }
}

