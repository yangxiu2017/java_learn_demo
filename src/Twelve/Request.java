package Twelve;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 封装request
 * @author lenovo
 *
 */

public class Request {
	//请求方式
	private String method;
	//请求资源
	private String url;
	//请求参数
	private Map<String,List<String>>parameterMapValues;
	
	//内部
	public static final String CRLF="/r/n";
	private InputStream is;
	private String requestInfo;
	
	public Request() {
		method ="";	
		url= "";
		parameterMapValues= new HashMap<String,List<String>>();
		requestInfo="";
}
	public Request(InputStream is) throws IOException {
		this();
		this.is = is;
		byte[]data = new byte[20480];
		int len = is.read(data);
		requestInfo = new String(data,0,len);
		return;
	}
	//分析请求信息
	 void parseRequestInfo() {
	}
	

 private void RequestInfo() {
	 if(null==requestInfo ||(requestInfo=requestInfo.trim()).equals("")) {
		 return;
	 }

    String paramString="";//接收请求参数
    //1 获取请求方式
    String firstLine = requestInfo.substring(0, requestInfo.indexOf(CRLF));
    int idx = requestInfo.indexOf("/");//  / 的位置 
    this.method = firstLine.substring(0,idx).trim();
    String urlStr = firstLine.substring(idx, firstLine.indexOf("HTTP/")).trim();
    if(this.method.equals("post")) {
    	this.url=urlStr;
    	paramString= requestInfo.substring(requestInfo.lastIndexOf(CRLF)).trim();
    	}else if(this.method.equals("get")) {
    		if(urlStr.contains("?")) {//是否存在参数
    			String[] urlArray=urlStr.split("\\?");
    			this.url= urlArray[0];
    			paramString = urlArray[1];//接收请求参数	
    		}else {
    			this.url= urlStr;
    		}
    	}
    //2 将请求参数封装到Map 中
 }
}