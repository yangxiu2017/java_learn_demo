package Mypro01;
/**
 * 测试switch结构
 * 遇到多值判断的时候，使用switch.当然，switch完全可以使用ifelseifelse代替。
 * @author lenovo
 *
 */
public class TestSwitch01 {
  public static void main(String[]args) {
	  char c = 'a';
	  int rand = (int)(26*Math.random());
	  char c2 = (char)(c + rand);
	  System.out.print(c2+":");
	  switch(c2) {
	  case 'a':
	  case 'e':
	  case 'i':
	  case 'o':
	  case 'u':
		System.out.println("元音");
		break;
	  case'y':
	  case 'w':
		  System.out.println("半元音");
		  break;
		 default:
			System.out.println("辅音");
			
			
	  }
  }
}
