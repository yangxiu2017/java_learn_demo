package Mypro01;

/**
 * 测试循环语句中的break
 * 
 * @author lenovo
 *
 */
public class TestBreak {
  public static void main(String[]args) {
	  int total = 0;//定义计数器
	  System.out.println("Begin");
	  while (true) {
		  total++;//每循环一次计数器加一
		  int i = (int)Math.round(100*Math.random());
		  System.out.println(i);
		  //当i等于88时，推出循环
		  if(i==88) {
			  break;
		  }
	  }
	  //输出循环的次数
	  System.out.println("Game over,use"+total+"times.");
  
  }
}
